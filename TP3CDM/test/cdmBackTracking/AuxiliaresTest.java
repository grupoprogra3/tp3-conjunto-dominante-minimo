package cdmBackTracking;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import operaciones.Grafo;

public class AuxiliaresTest 
{
	
	// CD = esConjuntoDominante
	// EH = esHoja
	
	@Test
	public void CDgrafoVacioTest() 
	{
		Grafo grafoVacio = new Grafo(0);
		
		Set<Integer> conjVacio = new HashSet<Integer>();
		
		assertTrue(Auxiliares.esConjuntoDominante(grafoVacio, conjVacio));
	}
	
	@Test
	public void CDunSoloVerticeTest() 
	{
		Grafo grafoUnVertice = new Grafo(1);
		
		Set<Integer> conj = new HashSet<Integer>();
		conj.add(0);
		
		assertTrue(Auxiliares.esConjuntoDominante(grafoUnVertice, conj));
	}
	
	@Test
	public void CDgrafoCompletoTest() 
	{
		Grafo grafoCompleto = completo();

		for(int i = 0; i < grafoCompleto.tamano(); i++) {
			Set<Integer> conj = new HashSet<Integer>();
			conj.retainAll(conj);
			conj.add(i);
			assertTrue(Auxiliares.esConjuntoDominante(grafoCompleto, conj));
		}
	}
	
	@Test
	public void CDconjuntoDominanteTest() 
	{
		Grafo grafo = ejemploTP();
		
		Set<Integer> conj = new HashSet<Integer>();
		conj.add(0);
		conj.add(3);
		
		assertTrue(Auxiliares.esConjuntoDominante(grafo, conj));
	}
	
	@Test
	public void CDnoConjuntoDominanteTest() 
	{
		Grafo grafo = ejemploTP();
		
		Set<Integer> conj = new HashSet<Integer>();
		conj.add(0);
		conj.add(5);
		
		assertFalse(Auxiliares.esConjuntoDominante(grafo, conj));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void EHgrafoVacio() 
	{
		Grafo grafoVacio = new Grafo(0);
		
		Auxiliares.esHoja(grafoVacio, 0);
	}
	
	@Test
	public void EHunSoloVerticeTest() 
	{
		Grafo grafoUnVertice = new Grafo(1);
		
		assertFalse(Auxiliares.esHoja(grafoUnVertice, 0));
	}
	
	@Test
	public void EHgrafoCompletoTest() 
	{
		Grafo grafoCompleto = completo();

		for(int i = 0; i < grafoCompleto.tamano(); i++) {
			assertFalse(Auxiliares.esHoja(grafoCompleto, i));
		}
	}
	
	@Test
	public void EHhojaTest() 
	{
		Grafo grafo = ejemploTP();
		
		assertTrue(Auxiliares.esHoja(grafo, 5));
	}
	
	@Test
	public void EHnoHojaTest() 
	{
		Grafo grafo = ejemploTP();
		
		assertFalse(Auxiliares.esHoja(grafo, 0));
	}
	
	private Grafo ejemploTP() 
	{
		Grafo g = new Grafo(6);
		g.agregarArista(0, 1);
		g.agregarArista(0, 4);
		g.agregarArista(1, 4);
		g.agregarArista(1, 2);
		g.agregarArista(4, 3);
		g.agregarArista(3, 2);
		g.agregarArista(3, 5);
		
		return g;
	}
	
	private Grafo completo()
	{
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		
		return grafo;
	}
	
}
