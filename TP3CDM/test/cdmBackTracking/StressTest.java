package cdmBackTracking;

import java.util.Random;

import operaciones.Grafo;

public class StressTest 
{
	public static void main(String[] args)
	{
		for(int n = 2; n < 100; ++n)
		{
			long inicio = System.currentTimeMillis();
			
			SolverBT solver = new SolverBT(aleatorio(n));
			solver.resolver();
			
			long fin = System.currentTimeMillis();
			double tiempo = (fin - inicio) / 1000.0;
			
			System.out.println("n = " + n + ": " + tiempo + " seg.");
		}
	}
	
	private static Grafo aleatorio(int n)
	{
		Grafo grafo = new Grafo(n);
		Random random = new Random(0);
		
		for(int i = 0; i < n; ++i)
			for(int j = i + 1; j < n; ++j) if( random.nextDouble() < 0.3 )
				grafo.agregarArista(i, j);
		
		return grafo;		
	}
}
