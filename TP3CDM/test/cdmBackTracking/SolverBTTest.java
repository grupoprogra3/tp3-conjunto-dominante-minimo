package cdmBackTracking;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import operaciones.Grafo;

public class SolverBTTest 
{
	
	@Test
	public void aisladoTest()
	{
		Grafo grafoAislado = aislado();
		SolverBT solver = new SolverBT(grafoAislado);
		Set<Integer> obtenido = solver.resolver();
		
		assertEquals(grafoAislado.tamano(), obtenido.size());
	}	
	
	@Test
	public void completoTest()
	{
		SolverBT solver = new SolverBT(completo());
		Set<Integer> obtenido = solver.resolver();
		
		assertEquals(1, obtenido.size());
	}
	
	@Test
	public void backTrackingCDMTest() 
	{
		SolverBT solver = new SolverBT(ejemploTP());
		Set<Integer> obtenido = solver.resolver();
		
		assertEquals(2, obtenido.size());
	}
	
	@Test
	public void trianguloConAntenaTest()
	{
		SolverBT solver = new SolverBT(trianguloConAntena());
		Set<Integer> obtenido = solver.resolver();
		
		Set<Integer> esperado = new HashSet<Integer>();
		esperado.add(1);
		
		assertEquals(esperado, obtenido);
	}
	
	private Grafo aislado() 
	{
		Grafo aislado = new Grafo(5);
		
		return aislado;
	}

	private Grafo completo()
	{
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		
		return grafo;
	}
	
	private Grafo ejemploTP() 
	{
		Grafo g = new Grafo(6);
		g.agregarArista(0, 1);
		g.agregarArista(0, 4);
		g.agregarArista(1, 4);
		g.agregarArista(1, 2);
		g.agregarArista(4, 3);
		g.agregarArista(3, 2);
		g.agregarArista(3, 5);
		
		return g;
	}
	
	private Grafo trianguloConAntena() 
	{
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 1);
		
		return grafo;
	}
	
}
