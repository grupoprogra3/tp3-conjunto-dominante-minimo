package cdmGoloso;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Comparator;

import operaciones.Grafo;
import operaciones.Vertice;

public class SolverGolosoTest 
{

	@Test
	public void resolverPorVecinosTest() 
	{
		SolverGoloso solver = new SolverGoloso(ejemploTP(), new Comparator<Vertice>() 
		{
			@Override
			public int compare(Vertice uno, Vertice otro) 
			{
				return -uno.cantidadDeVecinos() + otro.cantidadDeVecinos();
			}
			
		});
		
		Solucion solucion = solver.resolver();
		
		assertEquals(2, solucion.getCantidadVertices());
		assertEquals(6, solucion.cantidadDeVerticesAlcanzados());
	}

	private Grafo ejemploTP() 
	{
		Grafo g = new Grafo(6);
		g.agregarArista(0, 1);
		g.agregarArista(0, 4);
		g.agregarArista(1, 4);
		g.agregarArista(1, 2);
		g.agregarArista(4, 3);
		g.agregarArista(3, 2);
		g.agregarArista(3, 5);
		
		return g;
	}
	
}
