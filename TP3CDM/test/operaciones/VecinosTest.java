package operaciones;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class VecinosTest
{
	@Test
	public void todosAisladosTest()
	{
		Grafo grafo = new Grafo(5);
		
		assertEquals(0, grafo.vecinos(2).size());
	}
	
	@Test
	public void verticeUniversalTest()
	{
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(1, 0);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		
		HashSet<Integer> esperado = new HashSet<Integer>();
		esperado.add(0);
		esperado.add(2);
		esperado.add(3);
		
		assertEquals(esperado, grafo.vecinos(1));
	}
	
	@Test
	public void verticeNormalTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 4);
		
		HashSet<Integer> esperado = new HashSet<Integer>();
		esperado.add(1);
		esperado.add(2);
		
		assertEquals(esperado, grafo.vecinos(3));
	}
	
	@Test
	public void vecinosTest()
	{
		Grafo grafo = diamanteConVerticeAislado();
		
		int[] esperado = {0, 3, 4};
		
		setsIguales(esperado, grafo.vecinos(2));
	}
	
	@Test
	public void vecinosVaciosTest()
	{
		Grafo grafo = diamanteConVerticeAislado();
		
		int[] esperado = {};
		
		setsIguales(esperado, grafo.vecinos(1));
	}
	
	@Test
	public void unSoloVecinoTest()
	{
		Grafo grafo = diamanteConVerticeAislado();
		grafo.eliminarArista(0, 3);
		
		int[] esperado = {2};
		
		setsIguales(esperado, grafo.vecinos(0));
	}
	
	private void setsIguales(int[] esperado, Set<Integer> obtenido) 
	{
		for(Integer elemento: esperado)
			assertTrue(obtenido.contains(elemento));		
	}

	private Grafo diamanteConVerticeAislado()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 4);
		grafo.agregarArista(3, 4);
		
		return grafo;
	}

	
}

