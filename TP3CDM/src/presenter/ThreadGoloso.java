package presenter;

import java.util.concurrent.ExecutionException;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import cdmGoloso.ComparadorPorCantidadDeVecinos;
import cdmGoloso.Solucion;
import cdmGoloso.SolverGoloso;
import operaciones.Grafo;

public class ThreadGoloso extends SwingWorker<Solucion, Long>
{
	
	private JLabel _lblResultado;
	private JProgressBar _barraProgreso;
	private Grafo _grafo;
	
	public ThreadGoloso(JLabel lblResultado, JProgressBar barraProgreso, Grafo grafo) 
	{
		_lblResultado = lblResultado;
		_barraProgreso = barraProgreso;
		_grafo = grafo;
	}
	
	@Override
	protected Solucion doInBackground() throws Exception 
	{
		_barraProgreso.setIndeterminate(true);
		SolverGoloso solverGoloso = new SolverGoloso(_grafo, new ComparadorPorCantidadDeVecinos());
		return solverGoloso.resolver();
	}

	@Override
	public void done() 
	{
		try
		{
			if(this.isCancelled() == false) 
			{
				_lblResultado.setText(get().toString());
				_barraProgreso.setIndeterminate(false);
			}
		} catch(InterruptedException ex) {
			_lblResultado.setText("Interrumpido mientras ejecutaba el algoritmo");
			_barraProgreso.setIndeterminate(false);
		} catch(ExecutionException ex) {
			_lblResultado.setText("<html>Cargue un grafo antes de <br>ejecutar el algoritmo<html>");
			_barraProgreso.setIndeterminate(false);
		}
	}
	
}
