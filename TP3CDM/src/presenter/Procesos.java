package presenter;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.util.List;

import operaciones.Grafo;
import operaciones.Vertice;

public class Procesos 
{
	
	private List<Grafo> _historialGrafos;
	private Grafo _grafoActual;
	private ThreadGoloso threadGoloso;
	private ThreadBackTracking threadBT;
	
	public Procesos() 
	{
		_historialGrafos = ArchivoJSON.leerHistorialGrafos("HistorialGrafos.JSON");
	}

	// Metodos usados en VentanaPrincipal.
	public void cargarGrafoPredeterminado() 
	{
		_grafoActual = ArchivoJSON.leerGrafo("Grafo.JSON"); // El grafo predeterminado es el mismo que se da en la presentacion del TP3.
	}
	
	public void CDMGoloso(JLabel lblResultadoGoloso, JProgressBar barraDeProgreso) 
	{
		threadGoloso = new ThreadGoloso(lblResultadoGoloso, barraDeProgreso, _grafoActual);
		threadGoloso.execute();
	}

	public void CDMBackTracking(JLabel lblResultadoGoloso, JProgressBar barraDeProgreso)
	{
		threadBT = new ThreadBackTracking(lblResultadoGoloso, barraDeProgreso, _grafoActual);
		threadBT.execute();
	}
	
	// Metodos usados en VentanaCargarNuevoGrafo.
	public void setearGrafoNull()
	{
		_grafoActual = null;
	}
	
	public void cargarNuevoGrafo(int cantidadVertices) 
	{
		_grafoActual = new Grafo(cantidadVertices);
	}
	
	public void agregarArista(int verticeUno, int verticeDos) 
	{
		_grafoActual.agregarArista(verticeUno, verticeDos);
	}
	
	public boolean guardoGrafo() 
	{
		if(_grafoActual != null) 
		{
			_historialGrafos.add(_grafoActual);
			ArchivoJSON json = new ArchivoJSON();
			String jsonBasico = json.generarJSONBasico(_historialGrafos);
			json.guardarJSON(jsonBasico, "HistorialGrafos.JSON");
			return true;
		}
		return false;
	}
	
	// Metodos usados en VentanaHistorial.
	public void llenarTabla(DefaultTableModel modelo, JTable tablaHistorial) 
	{
		for(Grafo grafo : _historialGrafos) {
			Object[] fila = new Object[3];
			int numFila = _historialGrafos.indexOf(grafo);
			fila[0] = numFila + 1;
			fila[1] = grafo.tamano();
			fila[2] = informacion(grafo);
			modelo.addRow(fila);
		}
	}
	
	private String informacion(Grafo grafo) 
	{
		List<Vertice> vertices = grafo.getVertices();
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		for(Vertice vertice : vertices) {
			sb.append(vertice);
			if(vertice.getNumVertice() % 2 == 0) 
			{
				sb.append("<br>");	
			} else {
				sb.append(" - ");
			}
		}
		sb.append("<html>");
		return sb.toString();
	}

	public void acomodarTamanoColumna(JTable tablaHistorial) 
	{
		for(Grafo grafo : _historialGrafos) {
			List<Vertice> vertices = grafo.getVertices();
			int numFila = _historialGrafos.indexOf(grafo);
	 		for(Vertice vertice : vertices) {
				if(vertice.getNumVertice() % 2 == 0) 
				{
					tablaHistorial.setRowHeight(numFila, tablaHistorial.getRowHeight(numFila) + 15);
				}
			}
		}
	}
	
}
