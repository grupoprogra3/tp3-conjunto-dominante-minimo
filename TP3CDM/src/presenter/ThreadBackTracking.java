package presenter;

import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import cdmBackTracking.SolverBT;
import operaciones.Grafo;

public class ThreadBackTracking extends SwingWorker<Set<Integer>, Long>
{
	
	private JLabel _lblResultado;
	private JProgressBar _barraProgreso;
	private Grafo _grafo;
	
	public ThreadBackTracking(JLabel lblResultado, JProgressBar barraProgreso, Grafo grafo) 
	{
		_lblResultado = lblResultado;
		_barraProgreso = barraProgreso;
		_grafo = grafo;
	}
	
	@Override
	protected Set<Integer> doInBackground() throws Exception 
	{
		_barraProgreso.setIndeterminate(true);
		SolverBT solverBT = new SolverBT(_grafo);
		return solverBT.resolver();
	}

	@Override
	public void done() 
	{
		try
		{
			if(this.isCancelled() == false) 
			{
				_lblResultado.setText(get().toString());
				_barraProgreso.setIndeterminate(false);
			}
		} catch(InterruptedException ex) {
			_lblResultado.setText("Interrumpido mientras ejecutaba el algoritmo");
			_barraProgreso.setIndeterminate(false);
		} catch(ExecutionException ex) {
			_lblResultado.setText("<html>Cargue un grafo antes de <br>ejecutar el algoritmo<html>");
			_barraProgreso.setIndeterminate(false);
		}
	}
	
}
