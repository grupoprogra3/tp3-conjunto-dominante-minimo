package presenter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.List;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import operaciones.Grafo;

public class ArchivoJSON {

	public String generarJSONBasico(List<Grafo> _grafos) 
	{
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(_grafos);
		
		return json;
	}
	
	public void guardarJSON(String jsonParaGuardar, String archivoDestino) 
	{
		try 
		{
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(jsonParaGuardar);
			writer.close();
		} catch (Exception ex) 
		{
			ex.printStackTrace();
		}
	}
	
	public static Grafo leerGrafo(String archivo) 
	{
		Gson gson = new Gson();
		Grafo ret = null;
		
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, Grafo.class);
		} catch(Exception ex) 
		{
			ex.printStackTrace();
		}
		
		return ret;
	}
	
	public static ArrayList<Grafo> leerHistorialGrafos(String archivo) 
	{
		Gson gson = new Gson();
		ArrayList<Grafo> ret = null;
		Type historialGrafosTipo = new TypeToken<ArrayList<Grafo>>(){}.getType();
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, historialGrafosTipo);
		} catch(Exception ex) 
		{
			ex.printStackTrace();
		}
		
		return ret;
	}
}
