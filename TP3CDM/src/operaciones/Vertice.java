package operaciones;

import java.util.Set;
import java.util.HashSet;

public class Vertice {
	
	private int _numVertice;
	private Set<Integer> _vecinos;
	
	public Vertice(int numVertice) 
	{
		_numVertice = numVertice;
		_vecinos = new HashSet<Integer>();
	}
	
	public Set<Integer> getVecinos()
	{
		return _vecinos;
	}
	
	public int cantidadDeVecinos() 
	{
		return _vecinos.size();
	}
	
	public int getNumVertice() 
	{
		return _numVertice;
	}
	
	protected void vecindad (Set<Integer> vecinos) 
	{
		_vecinos = vecinos;
	}
	
	@Override
	public String toString() 
	{
		return "Vertice " + _numVertice + ", Vecinos: " + _vecinos;
	}
	
}
