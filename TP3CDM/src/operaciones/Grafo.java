package operaciones;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class Grafo {
	
	private boolean[][] _matrizAdyacencia;
	private ArrayList <Vertice> _vertices;
	
	public Grafo(int vertices) 
	{
		_matrizAdyacencia = new boolean [vertices][vertices];
		_vertices = new ArrayList<Vertice>();
		for(int i = 0; i < _matrizAdyacencia.length; i++) {
			Vertice v = new Vertice(i);
			_vertices.add(v);
		}
	}
	
	public int tamano()
	{
		return _matrizAdyacencia.length;
	}
	
	public Vertice getVertice(int i) 
	{
		return _vertices.get(i);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList <Vertice> getVertices()
	{
		return (ArrayList<Vertice>) _vertices.clone();
	}

	public void agregarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		Vertice v1 = getVertice(i);
		Vertice v2 = getVertice(j);
		
		_matrizAdyacencia[v1.getNumVertice()][v2.getNumVertice()] = true;
		_matrizAdyacencia[v2.getNumVertice()][v1.getNumVertice()] = true;
		
		v1.vecindad(vecinos(v1.getNumVertice()));
		v2.vecindad(vecinos(v2.getNumVertice()));
	}
	
	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		Vertice v1 = getVertice(i);
		Vertice v2 = getVertice(j);
		
		_matrizAdyacencia[v1.getNumVertice()][v2.getNumVertice()] = false;
		_matrizAdyacencia[v2.getNumVertice()][v1.getNumVertice()] = false;
		
		v1.vecindad(vecinos(v1.getNumVertice()));
		v2.vecindad(vecinos(v2.getNumVertice()));
	}
	
	protected Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		getVertice(i);

		Set<Integer> ret = new HashSet<Integer>();
		
		for(int j = 0; j < this.tamano(); j++) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(j);
		}
		
		return ret;		
	}

	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		Vertice v1 = getVertice(i);
		Vertice v2 = getVertice(j);

		return _matrizAdyacencia[v1.getNumVertice()][v2.getNumVertice()];
	}
	
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= _matrizAdyacencia.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}
	
	private void verificarDistintos(int i, int j)
	{
		if(i == j) 
		{
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
		}
	}

	public String toString() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append("========= Grafo ===========\n");
		for(Vertice vertice : _vertices) {
			sb.append(vertice);
			sb.append("\n");
		}
		sb.append("===========================");
		return sb.toString();
	}
	
}
