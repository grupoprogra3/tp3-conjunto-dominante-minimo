package cdmGoloso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import operaciones.Vertice;

public class Solucion 
{
	private ArrayList<Vertice> _vertices;
	
	public Solucion() 
	{
		_vertices = new ArrayList<Vertice>();
	}
	
	public int getCantidadVertices() 
	{
		return _vertices.size();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Vertice> getVertices()
	{
		return (ArrayList<Vertice>) _vertices.clone();
	}
	
	public int cantidadDeVerticesAlcanzados() 
	{
		int ret = 0;
		for(Vertice vertice: _vertices) {
			ret += vertice.cantidadDeVecinos();
		}
		
		return ret;
	}
	
	public void agregar(Vertice v) 
	{
		_vertices.add(v);
	}
	
	public boolean dominantes(Set<Integer> verVecinos) 
	{
		for(int i = 0; i<_vertices.size();i++) {
			if(verVecinos.contains(_vertices.get(i).getNumVertice())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() 
	{
		Set<Integer> ret = new HashSet<Integer>();
		for (Vertice vertice: _vertices) {
			ret.add(vertice.getNumVertice());
		}
		return ret.toString();
	}
	
}
