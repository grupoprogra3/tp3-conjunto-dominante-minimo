package cdmGoloso;

import java.util.Comparator;

import operaciones.Vertice;

public class ComparadorPorCantidadDeVecinos implements Comparator<Vertice> 
{
	@Override
	public int compare (Vertice uno, Vertice otro) 
	{
		return -uno.cantidadDeVecinos() + otro.cantidadDeVecinos();
	}
}
