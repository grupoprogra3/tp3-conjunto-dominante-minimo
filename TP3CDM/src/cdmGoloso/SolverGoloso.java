package cdmGoloso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import operaciones.Grafo;
import operaciones.Vertice;

public class SolverGoloso
{
	private Grafo _grafo;
	private Comparator <Vertice> _comparador;
	
	public SolverGoloso(Grafo grafo, Comparator<Vertice> comparador) 
	{
		_grafo = grafo;
		_comparador = comparador;
	}
	
	public Solucion resolver() 
	{
		Solucion ret = new Solucion();
		
		for(Vertice vertice : objetosOrdenados()) {
			if(ret.cantidadDeVerticesAlcanzados() + vertice.cantidadDeVecinos() <= _grafo.tamano() 
					&& !ret.dominantes(vertice.getVecinos())) 
			{
				ret.agregar(vertice);
			}
		}
		
		return ret;
	}
	
	public ArrayList<Vertice> objetosOrdenados()
	{
		ArrayList<Vertice> ret = _grafo.getVertices();
		Collections.sort(ret, _comparador);
		
		return ret;
	}
}
