package interfaz;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import presenter.Procesos;

@SuppressWarnings("serial")
public class VentanaCargarNuevoGrafo extends JFrame implements ActionListener
{
	private JPanel panelPrincipal;
	
	private JButton btnCargarGrafo;
	private JButton btnAgregarArista;
	private JButton btnGuardarGrafo;
	
	private JTextField txtFieldVertices;
	private JTextField txtFieldAristaVerticeUno;
	private JTextField txtFieldAristaVerticeDos;
	private JLabel lblVerticeUno;
	private JLabel lblVerticeDos;
	
	private ImageIcon tick;
	
	private Procesos programa;
	
	public VentanaCargarNuevoGrafo(Procesos programa) 
	{
		this.programa = programa;
		programa.setearGrafoNull();
		tick = new ImageIcon("src/interfaz/imagenes/tick.png");
		ImageIcon icono = new ImageIcon("src/interfaz/imagenes/icono.png");
		
		setResizable(false);
		setSize(400, 400);
		setTitle("Cargar nuevo grafo");
		setLocationRelativeTo(null);
		setIconImage(icono.getImage());
		getContentPane().setLayout(null);
		
		iniciarComponentes();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object botonQueSeApreta = e.getSource();
		if(botonQueSeApreta == btnCargarGrafo) 
		{
			try
			{
				Integer cantidadVertices = Integer.parseInt(txtFieldVertices.getText());
				programa.cargarNuevoGrafo(cantidadVertices);
				JOptionPane.showMessageDialog(null, "Se creado el grafo.", "Exito", JOptionPane.INFORMATION_MESSAGE, tick);
				
				txtFieldVertices.setText("");
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Debe ingresar un numero.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} 
		else if(botonQueSeApreta == btnAgregarArista) 
		{
			try 
			{
				Integer verticeUno = Integer.parseInt(txtFieldAristaVerticeUno.getText());
				Integer verticeDos = Integer.parseInt(txtFieldAristaVerticeDos.getText());
				programa.agregarArista(verticeUno, verticeDos);
				JOptionPane.showMessageDialog(null, "Arista agregada.", "Exito", JOptionPane.INFORMATION_MESSAGE, tick);
				
				txtFieldAristaVerticeUno.setText("");
				txtFieldAristaVerticeDos.setText("");
			} catch (NullPointerException ex) {
				JOptionPane.showMessageDialog(null, "Debe crear el grafo primero.", "Error", JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Debe completar ambos campos.", "Error", JOptionPane.ERROR_MESSAGE);
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(null, "- No se permiten loops.\n" +
													"- Los numeros deben ser menor a la cantidad de vertices del grafo.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		else if(botonQueSeApreta == btnGuardarGrafo) 
		{
			try 
			{
				if(programa.guardoGrafo() == true) 
				{
					JOptionPane.showMessageDialog(null, "Grafo guardado en el historial.", "Exito", JOptionPane.INFORMATION_MESSAGE, tick);
				}
			} catch (NullPointerException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private void iniciarComponentes() 
	{
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(Color.WHITE);
		panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPrincipal.setLayout(null);
		setContentPane(panelPrincipal);
		
		// JLabels
		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(10, 11, 50, 50);
		ImageIcon logo = new ImageIcon("src/interfaz/imagenes/logo.png");
		lblLogo.setIcon(logo);
		panelPrincipal.add(lblLogo);
		
		JLabel lblNakarias = new JLabel("Tecnologias NakaRias");
		lblNakarias.setBounds(61, 11, 154, 39);
		lblNakarias.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 15));
		panelPrincipal.add(lblNakarias);
		
		JLabel lblVertices = new JLabel("Tama�o del grafo");
		lblVertices.setBounds(71, 71, 187, 36);
		interfazLabel(lblVertices);
		
		JLabel lblAristas = new JLabel("Arista");
		lblAristas.setBounds(71, 197, 144, 36);
		interfazLabel(lblAristas);
		
		lblVerticeUno = new JLabel("Vertice 1");
		lblVerticeUno.setBounds(71, 238, 57, 20);
		lblVerticeUno.setFont(new Font("Yu Gothic", Font.BOLD, 12));
		panelPrincipal.add(lblVerticeUno);
		
		lblVerticeDos = new JLabel("Vertice 2");
		lblVerticeDos.setBounds(71, 269, 57, 20);
		lblVerticeDos.setFont(new Font("Yu Gothic", Font.BOLD, 12));
		panelPrincipal.add(lblVerticeDos);
		
		// TextFields
		txtFieldVertices = new JTextField();
		txtFieldVertices.setBounds(71, 106, 203, 20);
		interfazTextField(txtFieldVertices);
		
		txtFieldAristaVerticeUno = new JTextField();
		txtFieldAristaVerticeUno.setBounds(138, 232, 90, 20);
		interfazTextField(txtFieldAristaVerticeUno);
		
		txtFieldAristaVerticeDos = new JTextField();
		txtFieldAristaVerticeDos.setBounds(138, 263, 90, 20);
		interfazTextField(txtFieldAristaVerticeDos);
		
		// Botones
		btnCargarGrafo = new JButton("Crear grafo");
		btnCargarGrafo.setBounds(71, 150, 109, 30);
		interfazBoton(btnCargarGrafo);
		
		btnAgregarArista = new JButton("Agregar arista");
		btnAgregarArista.setBounds(71, 306, 115, 30);
		interfazBoton(btnAgregarArista);
		
		btnGuardarGrafo = new JButton("Guardar grafo");
		btnGuardarGrafo.setBounds(200, 306, 175, 30);
		interfazBoton(btnGuardarGrafo);
		
	}
	
	private void interfazBoton(JButton boton) 
	{
		boton.setForeground(Color.WHITE);
		boton.setBackground(new Color(0, 134, 190));
		boton.setBorder(null);
		boton.setFont(new Font("SansSerif", Font.BOLD, 14));
		boton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boton.addActionListener(this);
		panelPrincipal.add(boton);
	}
	
	private void interfazLabel(JLabel lblAtributo) 
	{
		lblAtributo.setForeground(Color.BLACK);
		lblAtributo.setFont(new Font("Yu Gothic", Font.BOLD, 18));
		panelPrincipal.add(lblAtributo);
	}
	
	private void interfazTextField(JTextField txtField) 
	{
		txtField.setBackground(Color.WHITE);
		txtField.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		txtField.setColumns(10);
		txtField.setFont(new Font("SansSerif", Font.PLAIN, 14));
		panelPrincipal.add(txtField);
	}
}
