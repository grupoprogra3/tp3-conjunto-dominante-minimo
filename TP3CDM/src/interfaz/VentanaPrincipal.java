package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JProgressBar;

import presenter.Procesos;

@SuppressWarnings("serial")
public class VentanaPrincipal extends JFrame implements ActionListener
{
	private JPanel panelPrincipal;
	
	private JButton btnCargarGrafo;
	private JButton btnCargarNuevo;
	private JButton btnHistorial;
	private JButton btnGoloso;
	private JButton btnBackTracking;

	private JLabel lblSolucionGoloso;
	private JLabel lblResultadoGoloso;
	private JLabel lblSolucionBT;
	private JLabel lblResultadoBT;
	
	private JProgressBar barraDeProgresoGoloso;
	private JProgressBar barraDeProgresoBT;
	
	private ImageIcon tick;

	private Procesos programa;
	
	public VentanaPrincipal()
	{
		tick = new ImageIcon("src/interfaz/imagenes/tick.png");
		programa = new Procesos();
		ImageIcon icono = new ImageIcon("src/interfaz/imagenes/icono.png");
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1024, 768);
		setTitle("Conjunto dominante minimo - Tecnologias NakaRias");
		setLocationRelativeTo(null);
		setIconImage(icono.getImage());
		try 
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		iniciarComponentes(); 
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object botonQueSeApreta = e.getSource();
		if(botonQueSeApreta == btnCargarGrafo) 
		{
			try
			{
				programa.cargarGrafoPredeterminado();
				JOptionPane.showMessageDialog(null, "Se ha cargado el grafo.", "Exito", JOptionPane.INFORMATION_MESSAGE, tick);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
		} 
		else if(botonQueSeApreta == btnCargarNuevo) 
		{
			JFrame ventana = new VentanaCargarNuevoGrafo(programa);
			ventana.setVisible(true);
		} 
		else if(botonQueSeApreta == btnGoloso) 
		{
			programa.CDMGoloso(lblResultadoGoloso, barraDeProgresoGoloso);
		} 
		else if(botonQueSeApreta == btnBackTracking)
		{
			programa.CDMBackTracking(lblResultadoBT, barraDeProgresoBT);
		}
		else if(botonQueSeApreta == btnHistorial) 
		{
			JFrame ventana = new VentanaHistorial(programa);
			ventana.setVisible(true);
		}
	}

	private void iniciarComponentes() 
	{
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(Color.WHITE);
		panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPrincipal.setLayout(null);
		setContentPane(panelPrincipal);
		
		// Botones
		btnCargarGrafo = new JButton("Cargar grafo predeterminado");
		btnCargarGrafo.setBounds(375, 121, 228, 28);
		interfazBoton(btnCargarGrafo);
		
		btnCargarNuevo = new JButton("Cargar nuevo grafo");
		btnCargarNuevo.setBounds(375, 178, 228, 28);
		interfazBoton(btnCargarNuevo);
		
		btnHistorial = new JButton("Historial");
		btnHistorial.setBounds(375, 235, 228, 28);
		interfazBoton(btnHistorial);
		
		btnGoloso = new JButton("Algoritmo goloso");
		btnGoloso.setBounds(179, 300, 228, 28);
		interfazBoton(btnGoloso);
		
		btnBackTracking = new JButton("BackTracking");
		btnBackTracking.setBounds(629, 300, 228, 28);
		interfazBoton(btnBackTracking);
		
		// JLabels
		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(10, 11, 50, 50);
		ImageIcon logo = new ImageIcon("src/interfaz/imagenes/logo.png");
		lblLogo.setIcon(logo);
		panelPrincipal.add(lblLogo);
		
		JLabel lblNakarias = new JLabel("Tecnologias NakaRias");
		lblNakarias.setBounds(61, 11, 154, 39);
		lblNakarias.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 15));
		panelPrincipal.add(lblNakarias);
		
		JLabel lblTituloCDM = new JLabel("Conjunto dominante minimo");
		lblTituloCDM.setBounds(317, 37, 373, 54);
		lblTituloCDM.setForeground(new Color(0, 134, 190));
		lblTituloCDM.setFont(new Font("Franklin Gothic Demi", Font.BOLD, 28));
		panelPrincipal.add(lblTituloCDM);
		
		JLabel lblUbicacion = new JLabel("Argentina, Buenos Aires 2023");
		lblUbicacion.setBounds(840, 18, 158, 14);
		lblUbicacion.setForeground(Color.BLACK);
		lblUbicacion.setFont(new Font("Franklin Gothic Demi", Font.PLAIN, 12));
		panelPrincipal.add(lblUbicacion);
		
		lblSolucionGoloso = new JLabel("Solucion");
		lblSolucionGoloso.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblSolucionGoloso.setBounds(254, 339, 77, 39);
		panelPrincipal.add(lblSolucionGoloso);
		
		lblSolucionBT = new JLabel("Solucion");
		lblSolucionBT.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblSolucionBT.setBounds(711, 339, 77, 28);
		panelPrincipal.add(lblSolucionBT);
		
		lblResultadoGoloso = new JLabel("");
		lblResultadoGoloso.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblResultadoGoloso.setBounds(179, 389, 305, 66);
		panelPrincipal.add(lblResultadoGoloso);
		
		lblResultadoBT = new JLabel("");
		lblResultadoBT.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblResultadoBT.setBounds(629, 389, 302, 66);
		panelPrincipal.add(lblResultadoBT);
		
		// Barras de progreso
		barraDeProgresoGoloso = new JProgressBar();
		barraDeProgresoGoloso.setBounds(145, 581, 339, 26);
		panelPrincipal.add(barraDeProgresoGoloso);
		
		barraDeProgresoBT = new JProgressBar();
		barraDeProgresoBT.setBounds(552, 581, 379, 26);
		panelPrincipal.add(barraDeProgresoBT);
	}
	
	private void interfazBoton(JButton boton) 
	{
		boton.setForeground(Color.WHITE);
		boton.setBackground(new Color(0, 134, 190));
		boton.setBorder(null);
		boton.setFont(new Font("SansSerif", Font.BOLD, 14));
		boton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		boton.addActionListener(this);
		panelPrincipal.add(boton);
	}
}
