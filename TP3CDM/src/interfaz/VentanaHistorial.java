package interfaz;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import presenter.Procesos;

@SuppressWarnings("serial")
public class VentanaHistorial extends JFrame {

	private JPanel panelPrincipal;
	private JTable tablaHistorial;
	private DefaultTableModel modelo;
	
	private Procesos programa;

	public VentanaHistorial(Procesos programa) 
	{
		this.programa = programa;
		ImageIcon icono = new ImageIcon("src/interfaz/imagenes/icono.png");
		
		setResizable(false);
		setSize(550, 400);
		setTitle("Historial");
		setLocationRelativeTo(null);
		setIconImage(icono.getImage());
		getContentPane().setLayout(null);
		
		iniciarComponentes();
	}

	private void iniciarComponentes() 
	{
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(Color.WHITE);
		panelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPrincipal.setLayout(null);
		setContentPane(panelPrincipal);
		
		JScrollPane scrollPaneTabla = new JScrollPane();
		scrollPaneTabla.setBounds(10, 11, 514, 339);
		panelPrincipal.add(scrollPaneTabla);
		
		tablaHistorial = new JTable();
		configurarModelo();
		configurarTabla();
		llenarTabla();
		acomodarTamanoColumna();
		scrollPaneTabla.setViewportView(tablaHistorial);
	}
	
	private void configurarModelo() 
	{
		modelo = (DefaultTableModel) tablaHistorial.getModel();
		modelo.addColumn("N� Grafo");
		modelo.addColumn("Tamano");
		modelo.addColumn("Informacion");
	}
	
	private void configurarTabla() 
	{
		tablaHistorial.setEnabled(false);
		tablaHistorial.getColumnModel().getColumn(0).setMaxWidth(55);
		tablaHistorial.getColumnModel().getColumn(1).setMinWidth(50);
		tablaHistorial.getColumnModel().getColumn(1).setMaxWidth(50);
	}
	
	private void llenarTabla() 
	{
		programa.llenarTabla(modelo, tablaHistorial);
	}
	
	private void acomodarTamanoColumna() 
	{
		programa.acomodarTamanoColumna(tablaHistorial);
	}
	
}
