package cdmBackTracking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import operaciones.Grafo;

public class SolverBT 
{
	private Grafo _grafo;
	private Set<Integer> _actual;
	private Set<Integer> _menor;
	private List<Set<Integer>> _soluciones;

	
	public SolverBT(Grafo grafo) 
	{
		_grafo = grafo;
	}
	
	public Set<Integer> resolver() 
	{
		_actual = new HashSet<Integer>();
		_menor = new HashSet<Integer>();
		for(int i = 0; i < _grafo.tamano(); i++) {
			_menor.add(i);
		}
		
		_soluciones = new ArrayList<Set<Integer>>();
		
		generarDesde(0);
		
		return _menor;
	}

	public List<Set<Integer>> getSoluciones()
	{
		return _soluciones;
	}

	private void generarDesde(int vertice) 
	{
		if(vertice == _grafo.tamano()) 
		{
			if(_actual.size() < _menor.size() && Auxiliares.esConjuntoDominante(_grafo, _actual)) 
			{
				_soluciones.add(clonar(_actual));
				_menor = clonar(_actual);
			}
			return;
		} 
		if(Auxiliares.esHoja(_grafo, vertice)) 
		{
			_actual.remove(vertice);
			generarDesde(vertice+1);
			return;
		}
		_actual.add(vertice);
		generarDesde(vertice+1);
				
		_actual.remove(vertice);
		generarDesde(vertice+1);
	}
	
	private Set<Integer> clonar(Set<Integer> conjunto) 
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(Integer vertices : conjunto) {
			ret.add(vertices);
		}
		
		return ret;
	}

}
