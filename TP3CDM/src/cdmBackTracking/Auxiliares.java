package cdmBackTracking;

import java.util.HashSet;
import java.util.Set;

import operaciones.Grafo;

public class Auxiliares 
{
	protected static boolean esConjuntoDominante(Grafo grafo, Set<Integer> conjunto) 
	{	
		Set<Integer> verticesAlcanzados = new HashSet<Integer>();
		for(Integer i : conjunto) {
			verticesAlcanzados.add(i);
			verticesAlcanzados.addAll(grafo.getVertice(i).getVecinos());
		}
		
		return verticesAlcanzados.size() == grafo.tamano();
	}

	protected static boolean esHoja(Grafo grafo, int vertice) 
	{
		if(grafo.getVertice(vertice).cantidadDeVecinos() == 1) 
		{
			return true;
		}
		return false;
	}
}
